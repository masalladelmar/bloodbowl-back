RENAME TABLE `bloodbowl`.`estrellas` TO `bloodbowl`.`star_players`;
ALTER TABLE `star_players` CHANGE `nombre` `name` VARCHAR(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `star_players` CHANGE `habilidades` `skills` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `star_players` CHANGE `coste` `price` MEDIUMINT(8) UNSIGNED NOT NULL;
RENAME TABLE `bloodbowl`.`estrella_raza` TO `bloodbowl`.`star_player_race`;
ALTER TABLE `star_player_race` CHANGE `raza_id` `race_id` INT(11) NOT NULL;
ALTER TABLE `star_player_race` CHANGE `estrella_id` `star_id` INT(11) NOT NULL;
ALTER TABLE `posiciones` CHANGE `normal` `normal` SET('GENERALES','FUERZA','AGILIDAD','PASE','MUTACIONES','general','strength','agility','passing','mutation') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `posiciones` CHANGE `dobles` `doubles` SET('GENERALES','FUERZA','AGILIDAD','PASE','MUTACIONES','general','strength','agility','passing','mutation') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;