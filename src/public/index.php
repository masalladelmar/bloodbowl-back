<?php

require '../../vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\UsersMapper;
use \Bloodbowl\Helper;
use \Firebase\JWT\ExpiredException;

$tables['coaches'] = 'coaches';
$tables['comments'] = 'comments';
$tables['journeys'] = 'journeys';
$tables['links'] = 'links';
$tables['matches'] = 'matches';
$tables['players'] = 'players';
$tables['players_skills'] = 'players_skills';
$tables['players_characteristics'] = 'players_characteristics';
$tables['playoffs'] = 'playoffs';
$tables['positions'] = 'positions';
$tables['positions_skills'] = 'positions_skills';
$tables['posts'] = 'posts';
$tables['races'] = 'races';
$tables['skills'] = 'skills';
$tables['stars'] = 'star_players';
$tables['stars_races'] = 'star_players_race';
$tables['teams'] = 'teams';
$tables['tournaments'] = 'tournaments';
$tables['tournaments_players'] = 'tournaments_players';
$tables['tournaments_teams'] = 'tournaments_teams';
$tables['users'] = 'users';

$config['db']['host']   = '127.0.0.1';
$config['db']['user']   = 'bloodbowl';
$config['db']['pass']   = 'bloodbowl';
$config['db']['dbname'] = 'bloodbowl';
$config['displayErrorDetails'] = true;

$server_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
$server_route = 'bloodbowlapi/src/public/';

$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO(
        'mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
        $db['user'],
        $db['pass']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->exec('set names utf8');
    return $pdo;
};

$container['logger'] = function ($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

/**
 * Middleware para verificación de token de autorización de usuario
 */
$mw = function ($request, $response, $next) use($container) {
    if ($request->hasHeader('HTTP_AUTHORIZATION')) {
        $token = $request->getHeader('HTTP_AUTHORIZATION');
        $helper = new Helper();
        try{
            $payload = $helper->verifyToken(str_replace('Bearer ', '', $token[0]));
        }
        catch(ExpiredException $e) {
            $container['logger']->addError('Token expirado: ' . $token[0]);
            return $response->withStatus(401);
        }
        if ($payload) {
            // Verificación correcta del token de autorización
            $request = $request->withAttribute('user', $payload->user_id);

            $response = $next($request, $response);

            return $response;
        }
        else {
            $container['logger']->addError('Not verified API token: ' . print_r($payload, true));
            return $response->withStatus(401);
        }
    }
    else {
        $container['logger']->addError('Authorization API header not present');
        return $response->withStatus(401);
    }

};

$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

$app->add(function (Request $req, Response $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

require '../routes/coaches.php';
require '../routes/teams.php';
require '../routes/tournaments.php';
require '../routes/posts.php';
require '../routes/races.php';
require '../routes/skills.php';
require '../routes/players.php';
require '../routes/starplayers.php';
require '../routes/links.php';
require '../routes/positions.php';
require '../routes/comments.php';
require '../routes/users.php';

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();
