<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\PostsMapper;
use \Bloodbowl\CommentsMapper;
use \Bloodbowl\Helper;

$routes = [
    'posts',
    'chronicles',
    'photos'
];

$serve_route = $server_host.$server_route.'uploads/posts/';
$upload_route = '../public/uploads/posts/';


foreach ($routes as $route) {
    $app->get("/{$route}", function(Request $request, Response $response, array $args) use($tables, $route) {
        $mapper = new PostsMapper($this->db, $this->logger, $tables);
        $items = array();

        $params = $request->getQueryParams();

        $type = substr($route, 0, -1);

        $items['results'] = $mapper->getPosts($type, $params);
        $items['total_items'] = $mapper->getCountPosts($type, $params);
        $newresponse = $response->withJson($items);
        return $newresponse;
    });

    $app->patch("/{$route}/{id}", function (Request $request, Response $response, array $args) use($tables) {
        $mapper = new PostsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
        $parsed = $request->getParsedBody();
        $data[':status'] = filter_var($parsed['status'], FILTER_SANITIZE_STRING);
        $data[':id'] = intval($args['id']);

        if ($mapper->publish($data))
        {
            return $response->withStatus(200);
        }
        else
        {
            return $response->withStatus(400);
        }
    })->add($mw);

    $app->get("/{$route}/{slug}", function (Request $request, Response $response, array $args) use($tables, $route, $serve_route) {
        $mapper = new PostsMapper($this->db, $this->logger, $tables);
        $params = $request->getQueryParams();

        if (is_numeric($args['slug'])) {
            $post = $mapper->getPostById(intval($args['slug'], 10), $params);
        }
        else {
            $post = $mapper->getPostBySlug($args['slug'], $params);
        }

        if (!$post)
        {
            return $response->withStatus(404);
        }

        $post['image'] = $post['archive'] ? $serve_route.$post['archive'] : '';

        if ($route == 'posts' || $route == 'photos') {
            $type = substr($route, 0, -1);
            $cmapper = new CommentsMapper($this->db, $this->logger, $tables);
            $post['previous'] = $mapper->getPrevious($type, $post['created_at'], $post['id']);
            $post['next'] = $mapper->getNext($type, $post['created_at'], $post['id']);
            $post['comments'] = $cmapper->getPostComments($post['id']);
        }
        $newresponse = $response->withJson($post);
        return $newresponse;
    });

    $app->post("/{$route}", function(Request $request, Response $response, array $args) use($tables, $route, $upload_route) {
        $mapper = new PostsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

        $type = substr($route, 0, -1);

        $parsed = $request->getParsedBody();
        $data = array();
        $helper = new Helper();

        if (isset($parsed['file']) && isset($parsed['filename'])) {
            $file = filter_var($parsed['file'], FILTER_SANITIZE_STRING);
            $filename = filter_var($parsed['filename'], FILTER_SANITIZE_STRING);
            $data[':archive'] = $helper->uploadFile($file, $filename, $upload_route);
        } else {
            $data[':archive'] = '';
        }
        
        $data[':comment_status'] = filter_var($parsed['comment_status'], FILTER_SANITIZE_STRING);
        $data[':content'] = filter_var($parsed['content'], FILTER_SANITIZE_STRING);
        $data[':match_id'] = filter_var($parsed['match_id'], FILTER_SANITIZE_NUMBER_INT);
        $data[':page_description'] = filter_var($parsed['page_description'], FILTER_SANITIZE_STRING);
        $data[':page_keywords'] = filter_var($parsed['page_keywords'], FILTER_SANITIZE_STRING);
        $data[':page_title'] = filter_var($parsed['page_title'], FILTER_SANITIZE_STRING);
        $data[':permalink'] = filter_var($parsed['permalink'], FILTER_SANITIZE_STRING);
        $data[':status'] = filter_var($parsed['status'], FILTER_SANITIZE_STRING);
        $data[':title'] = filter_var($parsed['title'], FILTER_SANITIZE_STRING);
        $data[':teaser'] = substr(filter_var($parsed['title'], FILTER_SANITIZE_STRING), 0, 255);
        $data[':type'] = $type;

        if ($mapper->create($data))
        {
            return $response->withStatus(200);
        }
        else
        {
            return $response->withStatus(400);
        }
    })->add($mw);

    $app->put("/{$route}/{id}", function(Request $request, Response $response, array $args) use($tables, $route, $upload_route) {
        $mapper = new PostsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

        $type = substr($route, 0, -1);

        $parsed = $request->getParsedBody();
        $data = array();
        $helper = new Helper();

        if (isset($parsed['file']) && isset($parsed['filename'])) {
            $file = filter_var($parsed['file'], FILTER_SANITIZE_STRING);
            $filename = filter_var($parsed['filename'], FILTER_SANITIZE_STRING);
            $data[':archive'] = $helper->uploadFile($file, $filename, $upload_route);
        } else {
            $data[':archive'] = '';
        }

        $data[':id'] = intval($args['id']);
        $data[':comment_status'] = filter_var($parsed['comment_status'], FILTER_SANITIZE_STRING);
        $data[':content'] = filter_var($parsed['content'], FILTER_SANITIZE_STRING);
        $data[':match_id'] = filter_var($parsed['match_id'], FILTER_SANITIZE_NUMBER_INT);
        $data[':page_description'] = filter_var($parsed['page_description'], FILTER_SANITIZE_STRING);
        $data[':page_keywords'] = filter_var($parsed['page_keywords'], FILTER_SANITIZE_STRING);
        $data[':page_title'] = filter_var($parsed['page_title'], FILTER_SANITIZE_STRING);
        $data[':permalink'] = filter_var($parsed['permalink'], FILTER_SANITIZE_STRING);
        $data[':status'] = filter_var($parsed['status'], FILTER_SANITIZE_STRING);
        $data[':title'] = filter_var($parsed['title'], FILTER_SANITIZE_STRING);
        $data[':teaser'] = substr(filter_var($parsed['title'], FILTER_SANITIZE_STRING), 0, 255);
        $data[':type'] = $type;

        if ($mapper->update($data))
        {
            return $response->withStatus(200);
        }
        else
        {
            return $response->withStatus(400);
        }
    })->add($mw);
}
