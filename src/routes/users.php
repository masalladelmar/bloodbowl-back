<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\UsersMapper;
use \Bloodbowl\Helper;

$gclient = new Google_Client(['client_id' => '179891062518-uh10mv7mu9c7f9pesq5brlhffob1gr1u.apps.googleusercontent.com']);

$app->post('/verify-user', function (Request $request, Response $response, array $args) use($tables, $gclient) {
    $data = $request->getParsedBody();
    $id_token = filter_var($data['id_token'], FILTER_SANITIZE_STRING);

    $payload = $gclient->verifyIdToken($id_token);
    if ($payload) {
        // Verificación correcta del token de google
        $mapper = new UsersMapper($this->db, $this->logger, $tables);
        $user = $mapper->verifyUser($payload['email']);
        if ($user) {
            // Verificación correcta del usuario
            // Generar token de autorización
            $helper = new Helper();
            $token = $helper->generateToken($user['id']);
            $response = $response->withJson($token);

            return $response;
        }
        $this->logger->addError('Not verified user in db: ' . $payload['email']);
        return $response->withStatus(404);
    }
    else {
        $this->logger->addError('Not verified Google token: ' . print_r($payload, true));
        return $response->withStatus(401);
    }
});