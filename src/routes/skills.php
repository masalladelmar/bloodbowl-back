<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\SkillsMapper;

$app->get('/skills/{type}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new SkillsMapper($this->db, $this->logger, $tables);
    $skills = $mapper->getSkills($args['type']);
    $newresponse = $response->withJson($skills);
    return $newresponse;
});

$app->get('/skills', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new SkillsMapper($this->db, $this->logger, $tables);
    $skills = $mapper->getAll();
    $newresponse = $response->withJson($skills);
    return $newresponse;
});