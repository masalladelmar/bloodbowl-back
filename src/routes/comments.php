<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\CommentsMapper;

$app->post('/comment/{post_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CommentsMapper($this->db, $this->logger, $tables);
    $data = $request->getParsedBody();
    $comment = array();
    $comment[':post_id'] = filter_var($args['post_id'], FILTER_SANITIZE_NUMBER_INT);
    $comment[':author'] = filter_var($data['author'], FILTER_SANITIZE_STRING);
    $comment[':content'] = filter_var($data['content'], FILTER_SANITIZE_STRING);
    if ($mapper->newComment($comment))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
});