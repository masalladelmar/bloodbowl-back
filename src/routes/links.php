<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\LinksMapper;

$app->get('/links', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new LinksMapper($this->db, $this->logger, $tables);
    $links = $mapper->getLinks();
    $newresponse = $response->withJson($links);
    return $newresponse;
});

$app->delete('/links/{id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new LinksMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->delete($args['id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        $this->logger->addError('Error en delete link');
        return $response->withStatus(400);
    }
})->add($mw);

$app->post('/links', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new LinksMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':description'] = filter_var($parsed['description'], FILTER_SANITIZE_STRING);
    $data[':url'] = filter_var($parsed['url'], FILTER_SANITIZE_URL);

    if ($mapper->create($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/links/{id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new LinksMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':description'] = filter_var($parsed['description'], FILTER_SANITIZE_STRING);
    $data[':url'] = filter_var($parsed['url'], FILTER_SANITIZE_URL);
    $data[':id'] = intval($args['id']);

    if ($mapper->update($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->patch('/links/{id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new LinksMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':published'] = filter_var($parsed['published'], FILTER_SANITIZE_NUMBER_INT);
    $data[':id'] = intval($args['id']);

    if ($mapper->publish($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);