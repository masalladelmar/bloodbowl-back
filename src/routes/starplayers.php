<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\StarPlayersMapper;

$app->get('/star-players/{race_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new StarPlayersMapper($this->db, $this->logger, $tables);
    $players = $mapper->getRaceStarPlayers($args['race_id']);
    $newresponse = $response->withJson($players);
    return $newresponse;
});