<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\PlayersMapper;
use \Bloodbowl\TeamsMapper;


$app->get('/teams/coach/{coach_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TeamsMapper($this->db, $this->logger, $tables);
    $pmapper = new PlayersMapper($this->db, $this->logger, $tables);
    $teams = $mapper->getTeamsByCoach($args['coach_id']);
    for($i = 0; $i < count($teams); $i++) {
        $teams[$i]['players'] = $pmapper->getTeamPlayers($teams[$i]['id']);
    }
    $newresponse = $response->withJson($teams);
    return $newresponse;
});

$app->get('/teams/{team}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TeamsMapper($this->db, $this->logger, $tables);
    $pmapper = new PlayersMapper($this->db, $this->logger, $tables);
    if (is_numeric($args['team'])) {
        $team = $mapper->getTeamById(intval($args['team'], 10));
    }
    else {
        $team = $mapper->getTeam($args['team']);
    }
    $team['players'] = $pmapper->getTeamPlayers($team['id']);
    $newresponse = $response->withJson($team);
    return $newresponse;
});

$app->get('/teams', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TeamsMapper($this->db, $this->logger, $tables);
    $teams = $mapper->getTeams();
    $newresponse = $response->withJson($teams);
    return $newresponse;
});