<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\PlayersMapper;
use \Bloodbowl\TeamsMapper;
use \Bloodbowl\MatchesMapper;
use \Bloodbowl\JourneysMapper;
use \Bloodbowl\TournamentsMapper;
use \Bloodbowl\PostsMapper;

$app->get('/tournaments/{tournament_id}/teams/{team_id}/matches', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $matches = $mapper->getMatchesByTeam($args['tournament_id'], $args['team_id']);
    $newresponse = $response->withJson($matches);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/teams', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TeamsMapper($this->db, $this->logger, $tables);
    $tournament = $mapper->getTeamsByTournament($args['tournament_id']);
    $newresponse = $response->withJson($tournament);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/experience', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables);
    $exp = array();
    $exp['actives'] = $mapper->getExperienceActives($args['tournament_id']);
    $exp['inactives'] = $mapper->getExperienceInactives($args['tournament_id']);
    $newresponse = $response->withJson($exp);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/journeys/{journey}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $matches = $mapper->getJourneyMatches($args['tournament_id'], $args['journey']);
    $newresponse = $response->withJson($matches);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/journeys', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new JourneysMapper($this->db, $this->logger, $tables);
    $journeys = $mapper->getJourneys($args['tournament_id']);
    $newresponse = $response->withJson($journeys);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/matches/{match_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $pmapper = new PostsMapper($this->db, $this->logger, $tables);
    $match = $mapper->getMatch($args['tournament_id'], $args['match_id']);
    if ($match) {
        $match['posts'] = $pmapper->getMatchPosts($args['match_id']);
    }
    $newresponse = $response->withJson($match);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/matches', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $journeys = $mapper->getMatches($args['tournament_id']);
    $newresponse = $response->withJson($journeys);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/stats', function (Request $request, Response $response, array $args) use($tables) {
    $tmapper = new TeamsMapper($this->db, $this->logger, $tables);
    $pmapper = new PlayersMapper($this->db, $this->logger, $tables);
    $mmapper = new MatchesMapper($this->db, $this->logger, $tables);
    $stats = array();
    $stats['players'] = array(
        array('key' => 'td', 'title' => 'Touch Downs', 'header' => 'Máximos anotadores'),
        array('key' => 'com', 'title' => 'Pases completos', 'header' => 'Los mejores pasadores'),
        array('key' => 'itc', 'title' => 'Intercepciones', 'header' => 'Los mejores interceptores'),
        array('key' => 'cas', 'title' => 'Bajas', 'header' => 'Los más sanguinarios'),
        array('key' => 'spp', 'title' => 'Puntos de experiencia', 'header' => 'Los más experimentados'),
        array('key' => 'exp', 'title' => 'Veces expulsado', 'header' => 'Los más faltosos')
    );

    foreach ($stats['players'] as $key => $value) {
        $stats['players'][$key]['data'] = $pmapper->getPlayersStats($args['tournament_id'], $value['key']);
    }
    
    $stats['teams'] = array(
        array('key' => 'cas_for', 'header' => 'Los equipos más sanguinarios', 'title' => 'Bajas a favor', 'order' => 'DESC'),
        array('key' => 'cas_against', 'header' => 'Los equipos con menos bajas', 'title' => 'Bajas en contra', 'order' => 'ASC'),
        array('key' => 'td_for', 'header' => 'Los equipos más anotadores', 'title' => 'TD a favor', 'order' => 'DESC'),
        array('key' => 'td_against', 'header' => 'Los equipos menos "goleados"', 'title' => 'TD en contra', 'order' => 'ASC')
    );

    foreach ($stats['teams'] as $key => $value) {
        $stats['teams'][$key]['data'] = $tmapper->getTeamsStats($args['tournament_id'], $value['key'], $value['order']);
    }
    
    $stats['matches'] = array(
        array('key' => 'winnings_1+winnings_2', 'header' => 'Record de recaudación en un encuentro', 'title' => 'Recaudación'),
        array('key' => 'cas_1+cas_2', 'header' => 'Record de bajas en un encuentro', 'title' => 'Bajas'),
        array('key' => 'td_1+td_2', 'header' => 'Record de touch downs en un encuentro', 'title' => 'Touch Downs'),
        array('key' => 'fans', 'header' => 'Record de aforo en un encuentro', 'title' => 'Aforo'),
    );

    foreach ($stats['matches'] as $key => $value) {
        $stats['matches'][$key]['data'] = $mmapper->getMatchesStats($args['tournament_id'], $value['key']);
    }
    
    $newresponse = $response->withJson($stats);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/ranking', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TeamsMapper($this->db, $this->logger, $tables);
    $ranking = $mapper->getRanking($args['tournament_id']);
    $newresponse = $response->withJson($ranking);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/playoffs/{match_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $pmapper = new PostsMapper($this->db, $this->logger, $tables);
    $playoff = $mapper->getPlayoffMatch($args['tournament_id'], $args['match_id']);
    if ($playoff) {
        $playoff['posts'] = $pmapper->getMatchPosts($args['match_id']);
    }
    $newresponse = $response->withJson($playoff);
    return $newresponse;
});

$app->get('/tournaments/{tournament_id}/playoffs', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new MatchesMapper($this->db, $this->logger, $tables);
    $playoffs = $mapper->getPlayoffs($args['tournament_id']);
    $newresponse = $response->withJson($playoffs);
    return $newresponse;
});

$app->get('/tournaments/{tournament}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TournamentsMapper($this->db, $this->logger, $tables);
    $tournament = $mapper->getTournament($args['tournament']);
    $newresponse = $response->withJson($tournament);
    return $newresponse;
});

$app->get('/tournaments', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new TournamentsMapper($this->db, $this->logger, $tables);
    $tournaments = $mapper->getTournaments();
    $newresponse = $response->withJson($tournaments);
    return $newresponse;
});