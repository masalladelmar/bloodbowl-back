<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\CoachesMapper;
use \Bloodbowl\TeamsMapper;

$app->get('/coaches', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CoachesMapper($this->db, $this->logger, $tables);
    $coaches = $mapper->getCoaches();
    $newresponse = $response->withJson($coaches);
    return $newresponse;
});

$app->get('/coaches-with-teams', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CoachesMapper($this->db, $this->logger, $tables);
    $tmapper = new TeamsMapper($this->db, $this->logger, $tables);
    $coaches = $mapper->getCoaches();
    for($i = 0; $i < count($coaches); $i++) {
        $coaches[$i]['teams'] = $tmapper->getTeamsByCoach($coaches[$i]['id']);
    }
    $newresponse = $response->withJson($coaches);
    return $newresponse;
});

$app->post('/coaches', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CoachesMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $data = $request->getParsedBody();
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
    if ($mapper->newCoach($name))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/coaches/{coach_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CoachesMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $data = $request->getParsedBody();
    $name = filter_var($data['name'], FILTER_SANITIZE_STRING);
    $coach_id = intval($args['coach_id']);
    if ($mapper->updateCoach($name, $coach_id))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->get('/coaches/{coach}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new CoachesMapper($this->db, $this->logger, $tables);
    if (is_numeric($args['coach'])) {
        $coach = $mapper->getCoachById(intval($args['coach'], 10));
    }
    else {
        $coach = $mapper->getCoach($args['coach']);
    }
    $newresponse = $response->withJson($coach);
    return $newresponse;
});