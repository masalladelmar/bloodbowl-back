<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\RacesMapper;
use \Bloodbowl\Helper;

$serve_route = $server_host.$server_route.'uploads/races/';
$upload_route = '../public/uploads/races/';

$app->get('/races/{race}', function (Request $request, Response $response, array $args) use($tables, $serve_route) {
    $mapper = new RacesMapper($this->db, $this->logger, $tables);
    if (is_numeric($args['race'])) {
        $race = $mapper->getRaceById(intval($args['race'], 10));
    }
    else {
        $race = $mapper->getRace($args['race']);
    }

    if (!$race)
    {
        return $response->withStatus(404);
    }

    $race['coat_arms'] = $serve_route.$race['coat_arms'];

    $newresponse = $response->withJson($race);
    return $newresponse;
});

$app->post('/races', function (Request $request, Response $response, array $args) use($tables, $upload_route) {
    $mapper = new RacesMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();

    $helper = new Helper();
    $file = filter_var($parsed['file'], FILTER_SANITIZE_STRING);
    $filename = filter_var($parsed['filename'], FILTER_SANITIZE_STRING);
    $data[':coat_arms'] = $helper->uploadFile($file, $filename, $upload_route);

    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':reroll_cost'] = filter_var($parsed['reroll_cost'], FILTER_SANITIZE_NUMBER_INT);
    $data[':description'] = filter_var($parsed['description'], FILTER_SANITIZE_STRING);
    $data[':apothecary'] = filter_var($parsed['apothecary'], FILTER_SANITIZE_NUMBER_INT);

    if ($mapper->newRace($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/races/{race_id}', function (Request $request, Response $response, array $args) use($tables, $upload_route) {
    $mapper = new RacesMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data = array();

    $helper = new Helper();
    $file = filter_var($parsed['file'], FILTER_SANITIZE_STRING);
    $filename = filter_var($parsed['filename'], FILTER_SANITIZE_STRING);
    $data[':coat_arms'] = $helper->uploadFile($file, $filename, $upload_route);

    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':reroll_cost'] = filter_var($parsed['reroll_cost'], FILTER_SANITIZE_NUMBER_INT);
    $data[':description'] = filter_var($parsed['description'], FILTER_SANITIZE_STRING);
    $data[':apothecary'] = filter_var($parsed['apothecary'], FILTER_SANITIZE_NUMBER_INT);
    $data[':id'] = intval($args['race_id']);

    if ($mapper->updateRace($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->get('/races', function (Request $request, Response $response, array $args) use($tables, $serve_route) {
    $mapper = new RacesMapper($this->db, $this->logger, $tables);
    $races = $mapper->getRaces();

    if ($races) {
        foreach ($races as &$race) {
            $race['coat_arms'] = $serve_route.$race['coat_arms'];
        }
    }

    $newresponse = $response->withJson($races);
    return $newresponse;
});