<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\PlayersMapper;

$app->get('/players/dead', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables);
    $players = $mapper->getDead();
    $newresponse = $response->withJson($players);
    return $newresponse;
});

$app->get('/players/famous', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables);
    $stats = array(
        array('key' => 'td', 'title' => 'Touch Downs', 'header' => 'El Pichichi <small>(r&eacute;cord de touch downs)</small>'),
        array('key' => 'com', 'title' => 'Pases completos', 'header' => 'Ojo de &Aacute;guila <small>(r&eacute;cord de pases completos)</small>'),
        array('key' => 'itc', 'title' => 'Intercepciones', 'header' => 'La Mano M&aacute;s Grande <small>(r&eacute;cord de intercepciones)</small>'),
        array('key' => 'cas', 'title' => 'Bajas', 'header' => 'El Asesino <small>(r&eacute;cord de bajas)</small>'),
        array('key' => 'spp', 'title' => 'Puntos de experiencia', 'header' => 'El Campe&oacute;n <small>(r&eacute;cord de puntos de experiencia)</small>'),
        array('key' => 'mvp', 'title' => 'Jugador del encuentro', 'header' => 'El Ídolo <small>(r&eacute;cord de mejor jugador del encuentro)</small>')
    );

    foreach ($stats as $key => $value) {
        $stats[$key]['data'] = $mapper->getFamous($value['key']);
    }
    $newresponse = $response->withJson($stats);
    return $newresponse;
});

$app->get('/teams/{team_id}/players', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables);
    $players = $mapper->getTeamPlayers($args['team_id']);

    $newresponse = $response->withJson($players);
    return $newresponse;
});

$app->put('/players/{player_id}/kill', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->killPlayer($args['player_id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/players/{player_id}/fire', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->firePlayer($args['player_id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/players/{player_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':number'] = filter_var($parsed['number'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ma'] = filter_var($parsed['ma'], FILTER_SANITIZE_NUMBER_INT);
    $data[':st'] = filter_var($parsed['st'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ag'] = filter_var($parsed['ag'], FILTER_SANITIZE_NUMBER_INT);
    $data[':av'] = filter_var($parsed['av'], FILTER_SANITIZE_NUMBER_INT);
    $data[':status'] = filter_var($parsed['status'], FILTER_SANITIZE_STRING);
    $data[':value'] = filter_var($parsed['value'], FILTER_SANITIZE_NUMBER_INT);
    $data[':id'] = intval($args['player_id']);

    if ($mapper->update($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->post('/players/{player_id}/modifiers', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':player_id'] = filter_var($args['player_id'], FILTER_SANITIZE_NUMBER_INT);
    $data[':type'] = filter_var($parsed['type'], FILTER_SANITIZE_STRING);
    $data[':modifier'] = filter_var($parsed['modifier'], FILTER_SANITIZE_NUMBER_INT);

    if ($id = $mapper->newModifier($data))
    {
        $newresponse = $response->withJson($id);
        return $newresponse->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->delete('/players/{player_id}/modifiers/{modifier_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->deleteModifier($args['player_id'], $args['modifier_id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->post('/players/{player_id}/skills', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':player_id'] = filter_var($args['player_id'], FILTER_SANITIZE_NUMBER_INT);
    $data[':skill_id'] = filter_var($parsed['skill_id'], FILTER_SANITIZE_NUMBER_INT);
    $data[':money_modifier'] = filter_var($parsed['money_modifier'], FILTER_SANITIZE_NUMBER_INT);

    if ($mapper->newSkill($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->delete('/players/{player_id}/skills/{skill_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PlayersMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->deleteSkill($args['player_id'], $args['skill_id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);