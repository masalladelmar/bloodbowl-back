<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Bloodbowl\PositionsMapper;

$app->get('/positions/{race_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables);
    $positions = $mapper->getPositions($args['race_id']);
    $newresponse = $response->withJson($positions);
    return $newresponse;
});

$app->post('/positions', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':limit'] = filter_var($parsed['limit'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ma'] = filter_var($parsed['ma'], FILTER_SANITIZE_NUMBER_INT);
    $data[':st'] = filter_var($parsed['st'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ag'] = filter_var($parsed['ag'], FILTER_SANITIZE_NUMBER_INT);
    $data[':av'] = filter_var($parsed['av'], FILTER_SANITIZE_NUMBER_INT);
    $skills = $parsed['skills'];
    $data[':normal'] = filter_var($parsed['normal'], FILTER_SANITIZE_STRING);
    $data[':doubles'] = filter_var($parsed['doubles'], FILTER_SANITIZE_STRING);
    $data[':price'] = filter_var($parsed['price'], FILTER_SANITIZE_NUMBER_INT);
    $data[':race_id'] = filter_var($parsed['race_id'], FILTER_SANITIZE_NUMBER_INT);
    $pos_id = $mapper->newPosition($data);
    if ($pos_id)
    {
        foreach ($skills as $skill) {
            $skill_data = array();
            $skill_data[':position_id'] = $pos_id;
            $skill_data[':skill_id'] = filter_var($skill['skill_id'], FILTER_SANITIZE_NUMBER_INT);
            $mapper->newSkill($skill_data);
        }
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/positions/{position_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data[':name'] = filter_var($parsed['name'], FILTER_SANITIZE_STRING);
    $data[':limit'] = filter_var($parsed['limit'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ma'] = filter_var($parsed['ma'], FILTER_SANITIZE_NUMBER_INT);
    $data[':st'] = filter_var($parsed['st'], FILTER_SANITIZE_NUMBER_INT);
    $data[':ag'] = filter_var($parsed['ag'], FILTER_SANITIZE_NUMBER_INT);
    $data[':av'] = filter_var($parsed['av'], FILTER_SANITIZE_NUMBER_INT);
    $data[':normal'] = filter_var($parsed['normal'], FILTER_SANITIZE_STRING);
    $data[':doubles'] = filter_var($parsed['doubles'], FILTER_SANITIZE_STRING);
    $data[':price'] = filter_var($parsed['price'], FILTER_SANITIZE_NUMBER_INT);
    $data[':id'] = intval($args['position_id']);
    if ($mapper->updatePosition($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->post('/positions/{position_id}/skills', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    $data = array();
    $data[':position_id'] = filter_var($args['position_id'], FILTER_SANITIZE_NUMBER_INT);
    $data[':skill_id'] = filter_var($parsed['skill_id'], FILTER_SANITIZE_NUMBER_INT);

    if ($mapper->newSkill($data))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->put('/races/{race_id}/positions/order', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));
    $parsed = $request->getParsedBody();
    if ($mapper->updateOrder($parsed))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);

$app->delete('/positions/{position_id}/skills/{skill_id}', function (Request $request, Response $response, array $args) use($tables) {
    $mapper = new PositionsMapper($this->db, $this->logger, $tables, $request->getAttribute('user'));

    if ($mapper->deleteSkill($args['position_id'], $args['skill_id']))
    {
        return $response->withStatus(200);
    }
    else
    {
        return $response->withStatus(400);
    }
})->add($mw);