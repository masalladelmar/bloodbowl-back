<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class LinksMapper
{
    private $database;
    private $tables;
    private $user;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->user = $user;
    }

    /**
     * Lista de enlaces para el menú
     */
    public function getLinks()
    {
        $sql = "SELECT * FROM {$this->tables['links']} WHERE deleted_at IS NULL";
        $result = $this->database->get($sql);

        return $result;
    }

    /**
     * Borrar un enlace
     */
    public function delete($id)
    {
        $sql = "DELETE FROM {$this->tables['links']} WHERE id=:id";
        return $this->database->set($sql, array(':id' => $id));
    }

    /**
     * Crear un enlace
     */
    public function create($data)
    {
        $sql = "INSERT INTO {$this->tables['links']} (name, description, url, published, created_by, created_at) VALUES (:name, :description, :url, 1, :user, CURRENT_TIMESTAMP)";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Actualizar un enlace
     */
    public function update($data)
    {
        $sql = "UPDATE {$this->tables['links']} SET name=:name, description=:description, url=:url, updated_by=:user, updated_at=CURRENT_TIMESTAMP WHERE id=:id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Cambiar publicación de un enlace
     */
    public function publish($data)
    {
        $sql = "UPDATE {$this->tables['links']} SET published=:published, updated_by=:user, updated_at=CURRENT_TIMESTAMP WHERE id=:id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }
}
