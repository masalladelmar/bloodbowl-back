<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class PositionsMapper
{
    private $database;
    private $tables;
    private $user;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->user = $user;
    }

    /**
     * Lista de posiciones de una raza
     */
    public function getPositions($race_id)
    {
        $sql = "SELECT * FROM {$this->tables['positions']} WHERE race_id = :race_id AND deleted_at IS NULL ORDER BY porder";
        $result = $this->database->get($sql, array(':race_id' => $race_id));

        if ($result) {
            $retorno = array();
            $skills = $this->getPositionsSkills(array_column($result, 'id'));
            foreach($result as $position) {
                $item = array(
                    'limit' => $position['plimit'],
                    'name' => $position['name'],
                    'ma' => $position['ma'],
                    'st' => $position['st'],
                    'ag' => $position['ag'],
                    'av' => $position['av'],
                    'skills_dep' => $position['skills'],
                    'normal' => $position['normal'],
                    'doubles' => $position['doubles'],
                    'price' => $position['price'],
                    'id' => $position['id'],
                    'order' => $position['porder']
                );

                $item['skills'] = array();
                if ($skills) {
                    for ($j = 0; $j < count($skills); $j++) {
                        if ($skills[$j]['position_id'] == $item['id']) {
                            $item['skills'][] = $skills[$j];
                        }
                    }
                }

                $retorno[] = $item;
            }
        } else {
            $retorno = $result;
        }

        return $retorno;
    }

    /**
     * Reordenar posiciones
     */
    public function order($order)
    {
        $result = "";
        foreach($order as $item) {
            $sql = "UPDATE {$this->tables['positions']} SET porder = :order, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id = :id";
            $result = $this->database->set($sql, array(':order' => $item['order'], ':user' => $this->user, ':id' => $item['id']));
            if (!$result) {
                return FALSE;
            }
        }
        return $result;
    }

    /**
     * Crear una posición
     */
    public function newPosition($data)
    {
        $sql = "SELECT MAX(porder) as porder FROM {$this->tables['positions']} WHERE race_id = :race_id";
        $result = $this->database->get($sql, array(':race_id' => $data[':race_id']));
        $data[':porder'] = $result ? $result[0]['porder'] + 1 : 1;
        $sql = "INSERT INTO {$this->tables['positions']} (name, race_id, ma, st, ag, av, price, plimit, normal, doubles, porder, created_at, created_by)
                VALUES (:name, :race_id, :ma, :st, :ag, :av, :price, :limit, :normal, :doubles, :porder, CURRENT_TIMESTAMP, :user)";
        $data[':user'] = $this->user;
        $result = $this->database->set($sql, $data);
        if ($result) {
            $result = $this->database->get('SELECT LAST_INSERT_ID() AS id;');
        }

        return $result ? $result[0]['id'] : FALSE;
    }

    /**
     * Actualizar una posición
     */
    public function updatePosition($data)
    {
        $sql = "UPDATE {$this->tables['positions']} SET name = :name, ma = :ma, st = :st, ag = :ag, av = :av, price = :price, plimit = :limit, normal = :normal, doubles = :doubles, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id = :id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Lista de habilidades de un conjunto de posiciones
     */
    public function getPositionsSkills($position_ids)
    {
        $ids = implode(',', $position_ids);
        $sql = "SELECT ps.*, s.name_es as name FROM {$this->tables['positions_skills']} ps JOIN {$this->tables['skills']} s ON ps.skill_id = s.id WHERE position_id IN ($ids)";
        return $this->database->get($sql);
    }

    /**
     * Quitar habilidad de una posición
     */
    public function deleteSkill($position_id, $skill_id)
    {
        $sql = "DELETE FROM {$this->tables['positions_skills']} WHERE position_id=:position_id AND skill_id=:skill_id";
        return $this->database->set($sql, array(':position_id' => $position_id, ':skill_id' => $skill_id));
    }

    /**
     * Añadir habilidad a una posición
     */
    public function newSkill($data)
    {
        $sql = "INSERT INTO {$this->tables['positions_skills']} VALUES (:position_id, :skill_id)";
        return $this->database->set($sql, $data);
    }

    /**
     * Actualizar orden de las posiciones
     */
    public function updateOrder($items)
    {
        if (is_array($items)) {
            foreach ($items as $item) {
                $sql = "UPDATE {$this->tables['positions']} SET porder = :order, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id = :id";
                $item[':user'] = $this->user;
                if (!$this->database->set($sql, $item)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
