<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class TournamentsMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de torneos
     */
    public function getTournaments()
    {
        $sql = "SELECT * FROM {$this->tables['tournaments']} WHERE deleted_at IS NULL ORDER BY `begin`";
        return $this->database->get($sql);
    }

    /**
     * Detalles de un torneo
     */
    public function getTournament($permalink)
    {
        $sql = "SELECT * FROM {$this->tables['tournaments']} WHERE `permalink` = :permalink";
        $result = $this->database->get($sql, array(':permalink' => $permalink));
        return $result ? $result[0] : $result;
    }
}
