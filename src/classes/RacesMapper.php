<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class RacesMapper
{
    private $database;
    private $tables;
    private $user;
    private $helper;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->user = $user;
        $this->helper = new Helper();
    }

    /**
     * Lista de razas
     */
    public function getRaces()
    {
        $sql = "SELECT * FROM {$this->tables['races']} WHERE deleted_at IS NULL";
        return $this->database->get($sql);
    }

    /**
     * Detalles de una raza
     */
    public function getRace($permalink)
    {
        $sql = "SELECT * FROM {$this->tables['races']} WHERE permalink = :permalink AND deleted_at IS NULL";
        $result = $this->database->get($sql, array(':permalink' => $permalink));
        return $result ? $result[0] : $result;
    }

    /**
     * Detalles de una raza
     */
    public function getRaceById($id)
    {
        $sql = "SELECT * FROM {$this->tables['races']} WHERE id = :id AND deleted_at IS NULL";
        $result = $this->database->get($sql, array(':id' => $id));
        return $result ? $result[0] : $result;
    }

    /**
     * Crear una raza
     */
    public function newRace($data)
    {
        $sql = "INSERT INTO {$this->tables['races']} (name, reroll_cost, description, coat_arms, apothecary, permalink, created_at, created_by)
                VALUES (:name, :reroll_cost, :description, :coat_arms, :apothecary, :permalink, CURRENT_TIMESTAMP, :user)";
        $data[':permalink'] = $this->helper->slugify($data[':name']);
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Actualizar una raza
     */
    public function updateRace($data)
    {
        $sql = "UPDATE {$this->tables['races']} SET name = :name, reroll_cost = :reroll_cost, description = :description, coat_arms = :coat_arms, apothecary = :apothecary, permalink = :permalink, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id = :id";
        $data[':permalink'] = $this->helper->slugify($data[':name']);
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }
}