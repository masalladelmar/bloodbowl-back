<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class SkillsMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de habilidades por tipo
     */
    public function getSkills($type)
    {
        $sql = "SELECT * FROM {$this->tables['skills']} WHERE `type` = :type ORDER BY name_es";
        return $this->database->get($sql, array(':type' => $type));
    }

    /**
     * Lista de habilidades completa
     */
    public function getAll()
    {
        $sql = "SELECT * FROM {$this->tables['skills']} ORDER BY type, name_es";
        return $this->database->get($sql);
    }
}