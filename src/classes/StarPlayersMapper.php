<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class StarPlayersMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de jugadores estrella de una raza
     */
    public function getRaceStarPlayers($race_id)
    {
        $sql = "SELECT * FROM {$this->tables['stars']} t JOIN {$this->tables['stars_races']} p ON t.id = p.star_id WHERE race_id = :race_id";
        $result = $this->database->get($sql, array(':race_id' => $race_id));

        if ($result) {
            $retorno = array();
            foreach($result as $position) {
                $retorno[] = array(
                    'name' => $position['name'],
                    'ma' => $position['ma'],
                    'st' => $position['st'],
                    'ag' => $position['ag'],
                    'av' => $position['av'],
                    'skills' => $position['skills'],
                    'price' => $position['price']
                );
            }
        } else {
            $retorno = $result;
        }

        return $retorno;
    }
}
