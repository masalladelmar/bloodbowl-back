<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class JourneysMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de jornadas de un torneo
     */
    public function getJourneys($tournament_id)
    {
        $sql = "SELECT * FROM {$this->tables['journeys']} WHERE tournament_id = :tournament_id ORDER BY journey ASC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }
}
