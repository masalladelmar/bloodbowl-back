<?php

namespace Bloodbowl;

use PDO;

use \Monolog\Logger;

class Database
{
    public function __construct(PDO $connection, Logger $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function get($sql, $params = array())
    {
        $stmt = $this->connection->prepare($sql);

        if (sizeof($params)) {
            foreach ($params as $param=>$value) {
                if (is_int($value)) {
                    $stmt->bindValue($param, $value, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue($param, $value, PDO::PARAM_STR);
                }
            }
        }

        try {
            $retorno = $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->addError("Ha ocurrido un error SQL en la consulta: \n\t\t" . $sql . "\n\t\tError: " . $e->getMessage());
        }

        if ($stmt->rowCount() > 0) {
            $retorno = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $retorno = FALSE;
        }

        return $retorno;
    }

    public function set($sql, $params = array())
    {
        $stmt = $this->connection->prepare($sql);

        if (sizeof($params)) {
            foreach ($params as $param=>$value) {
                if (is_int($value)) {
                    $stmt->bindValue($param, $value, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue($param, $value, PDO::PARAM_STR);
                }
            }
        }

        try {
            return $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->addError("Ha ocurrido un error SQL en la consulta: \n\t\t" . $sql . "\n\t\tError: " . $e->getMessage());
        }
    }
}