<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class PlayersMapper
{
    private $database;
    private $tables;
    private $logger;
    private $user;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->logger = $logger;
        $this->user = $user;
    }

    /**
     * Lista de jugadores de un equipo
     */
    public function getTeamPlayers($team_id)
    {
        $sql = "SELECT a.*, b.name as position_name, b.id as position_id, SUM(td) AS td, SUM(com) AS com, SUM(itc) AS itc, SUM(cas) AS cas, SUM(mvp) AS mvp, SUM(spp) AS spp, SUM(`exp`) AS `exp`
                FROM {$this->tables['players']} a JOIN {$this->tables['positions']} b ON a.position_id = b.id LEFT JOIN {$this->tables['tournaments_players']} c ON a.id = c.player_id
                WHERE a.team_id = :team_id
                GROUP BY a.id ORDER BY `number` ASC";
        $result = $this->database->get($sql, array(':team_id' => $team_id));

        $skills = $this->getPlayersSkills(array_column($result, 'id'));
        $modifiers = $this->getPlayersModifiers(array_column($result, 'id'));

        for($i = 0; $i < count($result); $i++) {
            $result[$i]['skills'] = array();
            if ($skills) {
                for ($j = 0; $j < count($skills); $j++) {
                    if ($skills[$j]['player_id'] == $result[$i]['id']) {
                        $result[$i]['skills'][] = $skills[$j];
                    }
                }
            }

            $result[$i]['modifiers'] = array();
            if ($modifiers) {
                for ($j = 0; $j < count($modifiers); $j++) {
                    if ($modifiers[$j]['player_id'] == $result[$i]['id']) {
                        $result[$i]['modifiers'][] = $modifiers[$j];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Estadísticas del torneo
     */
    public function getPlayersStats($tournament_id, $field)
    {
        $sql = "SELECT t.`name` AS team, p.`name` AS `name`, (SELECT `name` FROM positions WHERE id = p.position_id) AS position, `$field` AS total, `status`
                FROM {$this->tables['tournaments_players']} as tp JOIN {$this->tables['players']} p ON tp.player_id = p.id JOIN {$this->tables['teams']} t ON t.id = p.team_id
                WHERE `$field` > 0 AND tournament_id = :tournament_id ORDER BY total DESC LIMIT 10";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Experiencia de los jugadores activos
     */
    public function getExperienceActives($tournament_id)
    {
        $sql = "SELECT t.id, t.name AS team, c.name AS coach, SUM(com) AS com, SUM(td) AS td, SUM(itc) AS itc, SUM(cas) AS cas, SUM(mvp) AS mvp, SUM(spp) AS spp
                FROM {$this->tables['tournaments_players']} tt JOIN {$this->tables['players']} p ON p.id = tt.player_id JOIN {$this->tables['teams']} t ON t.id = p.team_id JOIN {$this->tables['coaches']} c ON t.coach_id = c.id
                WHERE tt.tournament_id = :tournament_id AND status = 'active'
                GROUP BY t.id ORDER BY coach ASC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Experiencia de los jugadores inactivos
     */
    public function getExperienceInactives($tournament_id)
    {
        $sql = "SELECT t.id, t.name AS team, c.name AS coach, SUM(com) AS com, SUM(td) AS td, SUM(itc) AS itc, SUM(cas) AS cas, SUM(mvp) AS mvp, SUM(spp) AS spp
                FROM {$this->tables['tournaments_players']} tt JOIN {$this->tables['players']} p ON p.id = tt.player_id JOIN {$this->tables['teams']} t ON t.id = p.team_id JOIN {$this->tables['coaches']} c ON t.coach_id = c.id
                WHERE tt.tournament_id = :tournament_id AND status <> 'active'
                GROUP BY t.id ORDER BY coach ASC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Lista de jugadores muertos
     */
    public function getDead()
    {
        $sql = "SELECT p.*, SUM(com) AS com, SUM(td) AS td, SUM(itc) AS itc, SUM(cas) AS cas, SUM(mvp) AS mvp, SUM(spp) AS spp, SUM(exp) AS exp, c.name as coach, t.name as team, po.name as position
                FROM {$this->tables['players']} p LEFT JOIN {$this->tables['tournaments_players']} tp ON p.id = tp.player_id JOIN {$this->tables['teams']} t ON t.id = p.team_id JOIN {$this->tables['positions']} po ON p.position_id = po.id JOIN {$this->tables['coaches']} c ON t.coach_id = c.id
                WHERE status = 'dead' GROUP BY p.id ORDER BY team_id ASC";
        return $this->database->get($sql);
    }

    /**
     * Lista de jugadores famosos
     */
    public function getFamous($skill)
    {
        $sql = "SELECT p.*, SUM(com) AS com, SUM(td) AS td, SUM(itc) AS itc, SUM(cas) AS cas, SUM(mvp) AS mvp, SUM(spp) AS spp, SUM(exp) AS exp, c.name as coach, t.name as team, po.name as position
                FROM {$this->tables['players']} p JOIN {$this->tables['tournaments_players']} tp ON p.id = tp.player_id JOIN {$this->tables['teams']} t ON t.id = p.team_id JOIN {$this->tables['positions']} po ON p.position_id = po.id JOIN {$this->tables['coaches']} c ON t.coach_id = c.id
                WHERE status = 'active' GROUP BY p.id
                HAVING SUM(`{$skill}`) = (SELECT SUM(`{$skill}`) FROM {$this->tables['tournaments_players']} htp JOIN {$this->tables['players']} hp ON htp.player_id = hp.id WHERE status = 'active' GROUP BY player_id ORDER BY SUM(`{$skill}`) DESC LIMIT 1)";
        return $this->database->get($sql);
    }

    /**
     * Lista de habilidades de un conjunto de jugadores
     */
    public function getPlayersSkills($players_ids)
    {
        $ids = implode(',', $players_ids);
        $sql = "SELECT ps.*, s.name_es as name FROM {$this->tables['players_skills']} ps JOIN {$this->tables['skills']} s ON ps.skill_id = s.id WHERE player_id IN ($ids)";
        return $this->database->get($sql);
    }

    /**
     * Quitar habilidad de un jugador
     */
    public function deleteSkill($player_id, $skill_id)
    {
        $sql = "DELETE FROM {$this->tables['players_skills']} WHERE player_id=:player_id AND skill_id=:skill_id";
        return $this->database->set($sql, array(':player_id' => $player_id, ':skill_id' => $skill_id));
    }

    /**
     * Añadir habilidad a un jugador
     */
    public function newSkill($data)
    {
        $sql = "INSERT INTO {$this->tables['players_skills']} VALUES (:player_id, :skill_id, :money_modifier)";
        return $this->database->set($sql, $data);
    }

    /**
     * Lista de modificadores de características de un conjunto de jugadores
     */
    public function getPlayersModifiers($players_ids)
    {
        $ids = implode(',', $players_ids);
        $sql = "SELECT * FROM {$this->tables['players_characteristics']} WHERE player_id IN ($ids)";
        return $this->database->get($sql);
    }

    /**
     * Quitar modificador de característica de un jugador
     */
    public function deleteModifier($player_id, $modifier_id)
    {
        $sql = "DELETE FROM {$this->tables['players_characteristics']} WHERE player_id=:player_id AND id=:id";
        return $this->database->set($sql, array(':player_id' => $player_id, ':id' => $modifier_id));
    }

    /**
     * Añadir habilidad a un jugador
     */
    public function newModifier($data)
    {
        $sql = "INSERT INTO {$this->tables['players_characteristics']}(player_id, type, modifier) VALUES (:player_id, :type, :modifier)";
        $result = $this->database->set($sql, $data);
        if ($result) {
            $result = $this->database->get('SELECT LAST_INSERT_ID() AS id;');
        }

        return $result ? $result[0]['id'] : FALSE;
    }

    /**
     * Marcar a un jugador como muerto
     */
    public function killPlayer($player_id)
    {
        $sql = "UPDATE {$this->tables['players']} SET status='dead', updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id=:id";
        return $this->database->set($sql, array(':id' => $player_id, ':user' => $this->user));
    }

    /**
     * Marcar a un jugador como despedido
     */
    public function firePlayer($player_id)
    {
        $sql = "UPDATE {$this->tables['players']} SET status='fired', updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id=:id";
        return $this->database->set($sql, array(':id' => $player_id, ':user' => $this->user));
    }

    /**
     * Actualizar los datos de un jugador
     * TODO: comprobar que si un jugador cambia a estado activo, no haya otro que tenga el mismo dorsal y esté activo también
     */
    public function update($data)
    {
        $sql = "UPDATE {$this->tables['players']} SET name = :name, ma = :ma, st = :st, ag = :ag, av = :av, number = :number, status = :status, value = :value, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id=:id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }
}
