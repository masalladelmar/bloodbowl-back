<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class CoachesMapper
{
    private $database;
    private $tables;
    private $user;
    private $helper;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->user = $user;
        $this->helper = new Helper();
    }

    /**
     * Lista de entrenadores
     */
    public function getCoaches()
    {
        $sql = "SELECT * FROM {$this->tables['coaches']} WHERE deleted_at IS NULL ORDER BY name ASC";
        return $this->database->get($sql);
    }

    /**
     * Detalles de un entrenador
     */
    public function getCoach($permalink)
    {
        $sql = "SELECT * FROM {$this->tables['coaches']} WHERE permalink = :permalink AND deleted_at IS NULL";
        $result = $this->database->get($sql, array(':permalink' => $permalink));
        return $result ? $result[0] : $result;
    }

    /**
     * Detalles de un entrenador
     */
    public function getCoachById($id)
    {
        $sql = "SELECT * FROM {$this->tables['coaches']} WHERE id = :id AND deleted_at IS NULL";
        $result = $this->database->get($sql, array(':id' => $id));
        return $result ? $result[0] : $result;
    }

    /**
     * Crear un entrenador
     */
    public function newCoach($name)
    {
        $sql = "INSERT INTO {$this->tables['coaches']} (name, permalink, created_at, created_by)
                VALUES (:name, :permalink, CURRENT_TIMESTAMP, :user)";
        $permalink = $this->helper->slugify($name);
        return $this->database->set($sql, array(':name' => $name, ':permalink' => $permalink, ':user' => $this->user));
    }

    /**
     * Actualizar un entrenador
     */
    public function updateCoach($name, $id)
    {
        $sql = "UPDATE {$this->tables['coaches']} SET name = :name, permalink = :permalink, updated_at = CURRENT_TIMESTAMP, updated_by = :user WHERE id = :id";
        $permalink = $this->helper->slugify($name);
        return $this->database->set($sql, array(':name' => $name, ':permalink' => $permalink, ':user' => $this->user, ':id' => $id));
    }
}
