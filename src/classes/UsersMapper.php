<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class UsersMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Verificar un token de usuario
     */
    public function verifyUser($email)
    {
        $sql = "SELECT * FROM {$this->tables['users']} WHERE email = :email";
        $result = $this->database->get($sql, array(':email' => $email));
        return $result ? $result[0] : $result;
    }
}