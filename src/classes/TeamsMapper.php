<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class TeamsMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de equipos
     */
    public function getTeams()
    {
        $sql = "SELECT a.*, b.name as coach_name, c.name as race_name FROM {$this->tables['teams']} a JOIN {$this->tables['coaches']} b ON a.coach_id = b.id JOIN {$this->tables['races']} c ON a.race_id = c.id WHERE a.deleted_at IS NULL";
        return $this->database->get($sql);
    }

    /**
     * Lista de equipos por entrenador
     */
    public function getTeamsByCoach($coach_id)
    {
        $sql = "SELECT a.*, b.name as coach_name, c.name as race_name, c.reroll_cost FROM {$this->tables['teams']} a JOIN {$this->tables['coaches']} b ON a.coach_id = b.id JOIN {$this->tables['races']} c ON a.race_id = c.id WHERE a.coach_id = {$coach_id} AND a.deleted_at IS NULL";
        return $this->database->get($sql);
    }

    /**
     * Lista de equipos por torneo
     */
    public function getTeamsByTournament($tournament_id)
    {
        $sql = "SELECT a.*, b.name as coach_name, c.name as race_name FROM {$this->tables['teams']} a JOIN {$this->tables['coaches']} b ON a.coach_id = b.id JOIN {$this->tables['races']} c ON a.race_id = c.id JOIN {$this->tables['tournaments_teams']} d ON a.id = d.team_id WHERE a.deleted_at IS NULL AND d.tournament_id = {$tournament_id}";
        return $this->database->get($sql);
    }

    /**
     * Detalles de un equipo
     */
    public function getTeam($permalink)
    {
        $sql = "SELECT a.*, b.name as coach_name, c.name as race_name, c.reroll_cost FROM {$this->tables['teams']} a JOIN {$this->tables['coaches']} b ON a.coach_id = b.id JOIN {$this->tables['races']} c ON a.race_id = c.id JOIN {$this->tables['tournaments_teams']} d ON a.id = d.team_id WHERE a.deleted_at IS NULL AND a.permalink = :permalink";
        $result = $this->database->get($sql, array(':permalink' => $permalink));
        return $result ? $result[0] : $result;
    }

    /**
     * Detalles de una raza
     */
    public function getTeamById($id)
    {
        $sql = "SELECT a.*, b.name as coach_name, c.name as race_name, c.reroll_cost FROM {$this->tables['teams']} a JOIN {$this->tables['coaches']} b ON a.coach_id = b.id JOIN {$this->tables['races']} c ON a.race_id = c.id JOIN {$this->tables['tournaments_teams']} d ON a.id = d.team_id WHERE a.deleted_at IS NULL AND a.id = :id";
        $result = $this->database->get($sql, array(':id' => $id));
        return $result ? $result[0] : $result;
    }

    /**
     * Estadísticas del torneo
     */
    public function getTeamsStats($tournament_id, $field, $order)
    {
        $sql = "SELECT t.name, {$field} AS value
                FROM {$this->tables['tournaments_teams']} tt JOIN {$this->tables['teams']} t ON tt.team_id = t.id
                WHERE tt.tournament_id = :tournament_id ORDER BY value {$order} LIMIT 10";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Clasificación del torneo
     */
    public function getRanking($tournament_id)
    {
        $sql = "SELECT t.id, t.name, t.permalink, played, won, drawn, lost, points, tournament_group, value, td_for, td_against, cas_for, cas_against,
                CAST(td_for AS SIGNED)+CAST(cas_for AS SIGNED)-CAST(td_against AS SIGNED)-CAST(cas_against AS SIGNED) AS average
                FROM {$this->tables['tournaments_teams']} tt JOIN {$this->tables['teams']} t ON tt.team_id = t.id
                WHERE tournament_id = :tournament_id
                ORDER BY tournament_group ASC, points DESC, average DESC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }
}
