<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class PostsMapper
{
    private $database;
    private $tables;
    private $page_count = 10;
    private $user;
    private $helper;

    public function __construct(PDO $connection, Logger $logger, array $tables, $user = 0)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
        $this->user = $user;
        $this->helper = new Helper();
    }

    /**
     * Posts de un encuentro
     */
    public function getMatchPosts($match_id)
    {
        $sql = "SELECT * FROM {$this->tables['posts']}
                WHERE match_id = :match_id AND deleted_at IS NULL AND status = 'published'";
        return $this->database->get($sql, array(':match_id' => $match_id));
    }

    /**
     * Lista de posts de un tipo determinado
     */
    public function getPosts($type, $params)
    {

        $sql = "SELECT p.*, u.first_name as author FROM {$this->tables['posts']} p JOIN {$this->tables['users']} u ON p.created_by = u.id
                WHERE type = '{$type}'";

        if (isset($params['status'])) {
            $sql .= " AND status = '{$params['status']}'";
        }
        if (isset($params['deleted']) && $params['deleted'] == 'false') {
            $sql .= " AND deleted_at IS NULL";
        }
        if (isset($params['page'])) {
            $offset = (intval($params['page']) - 1) * $this->page_count;
        } else {
            $offset = 0;
        }
        $sql .= " ORDER BY created_at DESC LIMIT {$this->page_count} OFFSET :offset";
        return $this->database->get($sql, array(':offset' => $offset));
    }

    /**
     * Número de posts de un tipo determinado
     */
    public function getCountPosts($type, $params)
    {
        $sql = "SELECT COUNT(*) as total FROM {$this->tables['posts']}
                WHERE type = '{$type}'";
        if (isset($params['status'])) {
            $sql .= " AND status = '{$params['status']}'";
        }
        if (isset($params['deleted']) && $params['deleted'] == 'false') {
            $sql .= " AND deleted_at IS NULL";
        }
        $result = $this->database->get($sql);
        return $result ? $result[0]['total'] : 0;
    }

    /**
     * Cambiar publicación de un post
     */
    public function publish($data)
    {
        $sql = "UPDATE {$this->tables['posts']} SET status=:status, updated_by=:user, updated_at=CURRENT_TIMESTAMP WHERE id=:id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Detalle de un post por permalink
     */
    public function getPostBySlug($permalink, $params)
    {
        $sql = "SELECT p.*, u.first_name as author FROM {$this->tables['posts']} p JOIN {$this->tables['users']} u ON p.created_by = u.id
                WHERE permalink = :permalink";
        if (isset($params['status'])) {
            $sql .= " AND status = '{$params['status']}'";
        }
        if (isset($params['deleted']) && $params['deleted'] == 'false') {
            $sql .= " AND deleted_at IS NULL";
        }
        $result = $this->database->get($sql, array(':permalink' => $permalink));
        return $result ? $result[0] : $result;
    }

    /**
     * Detalle de un post por ID
     */
    public function getPostById($id, $params)
    {
        $sql = "SELECT p.*, u.first_name as author FROM {$this->tables['posts']} p JOIN {$this->tables['users']} u ON p.created_by = u.id
                WHERE p.id = :id";
        if (isset($params['status'])) {
            $sql .= " AND status = '{$params['status']}'";
        }
        if (isset($params['deleted']) && $params['deleted'] == 'false') {
            $sql .= " AND deleted_at IS NULL";
        }
        $result = $this->database->get($sql, array(':id' => $id));
        return $result ? $result[0] : $result;
    }

    /**
     * Post anterior
     */
    public function getPrevious($type, $date, $id)
    {
        $sql = "SELECT * FROM {$this->tables['posts']}
                WHERE type = '{$type}' AND created_at > '{$date}' AND id <> {$id} AND deleted_at IS NULL AND status = 'published'
                ORDER BY created_at ASC LIMIT 1";
        $result = $this->database->get($sql);
        return $result ? $result[0] : $result;
    }

    /**
     * Post siguiente
     */
    public function getNext($type, $date, $id)
    {
        $sql = "SELECT * FROM {$this->tables['posts']}
                WHERE type = '{$type}' AND created_at < '{$date}' AND id <> {$id} AND deleted_at IS NULL AND status = 'published'
                ORDER BY created_at DESC LIMIT 1";
        $result = $this->database->get($sql);
        return $result ? $result[0] : $result;
    }

    /**
     * Crear un post
     */
    public function create($data)
    {
        $sql = "INSERT INTO {$this->tables['posts']}(title, teaser, page_title, page_description, page_keywords, content, permalink, status, type, comment_status, archive, match_id, created_at, created_by)
                VALUES (:title, :teaser, :page_title, :page_description, :page_keywords, :content, :permalink, :status, :type, :comment_status, :archive, :match_id, CURRENT_TIMESTAMP, :user)";
        $data[':permalink'] = $this->helper->slugify($data[':permalink']);
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }

    /**
     * Actualizar un post
     */
    public function update($data)
    {
        $sql = "UPDATE {$this->tables['posts']} SET
                title=:title,
                teaser=:teaser,
                page_title=:page_title,
                page_description=:page_description,
                page_keywords=:page_keywords,
                content=:content,
                permalink=:permalink,
                status=:status,
                type=:type,
                comment_status=:comment_status,
                archive=:archive,
                match_id=:match_id,
                updated_by=:user,
                updated_at=CURRENT_TIMESTAMP
                WHERE id=:id";
        $data[':user'] = $this->user;
        return $this->database->set($sql, $data);
    }
}
