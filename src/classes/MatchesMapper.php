<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class MatchesMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Lista de encuentros de un torneo
     */
    public function getMatches($tournament_id)
    {
        $sql = "SELECT t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, m.*
                FROM {$this->tables['matches']} m LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2
                WHERE tournament_id = :tournament_id ORDER BY journey ASC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Detalle de un encuentro de un torneo
     */
    public function getMatch($tournament_id, $match_id)
    {
        $sql = "SELECT t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, m.*
                FROM {$this->tables['matches']} m LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2
                WHERE tournament_id = :tournament_id AND m.id = :match_id";
        $result = $this->database->get($sql, array(':tournament_id' => $tournament_id, ':match_id' => $match_id));
        return $result ? $result[0] : $result;
    }

    /**
     * Datos de un encuentro para un post
     */
    public function getMatchData($match_id)
    {
        $sql = "SELECT t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, t.name as tournament, t.permalink as tournament_permalink, m.*
                FROM {$this->tables['matches']} m LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2 LEFT JOIN {$this->tables['tournaments']} t ON t.id = m.tournament_id
                WHERE m.id = :match_id";
        $result = $this->database->get($sql, array(':match_id' => $match_id));
        return $result ? $result[0] : $result;
    }

    /**
     * Lista de encuentros de un equipo en un torneo
     */
    public function getMatchesByTeam($tournament_id, $team_id)
    {
        $sql = "SELECT *, (SELECT `name` FROM {$this->tables['teams']} WHERE id = {$this->tables['matches']}.team_id_1) AS name_1, (SELECT name FROM {$this->tables['teams']} WHERE id = {$this->tables['matches']}.team_id_2) AS name_2 FROM {$this->tables['matches']} WHERE tournament_id = :tournament_id AND (team_id_1 = :team_id1 OR team_id_2 = :team_id2) AND deleted_at IS NULL ORDER BY journey ASC";
        $params = array(
            ':tournament_id' => $tournament_id,
            ':team_id1' => $team_id,
            ':team_id2' => $team_id
        );
        return $this->database->get($sql, $params);
    }

    /**
     * Estadísticas del torneo
     */
    public function getMatchesStats($tournament_id, $field)
    {
        $sql = "SELECT journey, team_id_1, team_id_2, (SELECT name FROM {$this->tables['teams']} WHERE id = team_id_1) AS name_1, (SELECT name FROM {$this->tables['teams']} WHERE id = team_id_2) AS name_2, $field AS value
                FROM {$this->tables['matches']}
                WHERE updated = 1 AND tournament_id = :tournament_id ORDER BY value DESC LIMIT 10";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Encuentros de una jornada
     */
    public function getJourneyMatches($tournament_id, $journey)
    {
        $sql = "SELECT (SELECT COUNT(*) FROM {$this->tables['posts']} WHERE match_id = m.id AND type = 'photo' AND status = 'published') AS photos, (SELECT permalink FROM {$this->tables['posts']} WHERE match_id = m.id AND type = 'chronicle' AND status = 'published') AS chronicle, t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, m.*
                FROM {$this->tables['matches']} m LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2
                WHERE tournament_id = :tournament_id AND journey = :journey";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id, ':journey' => $journey));
    }

    /**
     * Encuentros de los playoffs de un torneo
     */
    public function getPlayoffs($tournament_id)
    {
        $sql = "SELECT t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, m.*
                FROM {$this->tables['playoffs']} m LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2
                WHERE tournament_id = :tournament_id ORDER BY phase ASC, journey ASC";
        return $this->database->get($sql, array(':tournament_id' => $tournament_id));
    }

    /**
     * Detalle de un encuentro de playoff de un torneo
     */
    public function getPlayoffMatch($tournament_id, $match_id)
    {
        $sql = "SELECT t1.name as team_name_1, t2.name as team_name_2, t1.permalink as team_permalink_1, t2.permalink as team_permalink_2, p.*
                FROM {$this->tables['playoffs']} p LEFT JOIN {$this->tables['teams']} t1 ON t1.id = team_id_1 LEFT JOIN {$this->tables['teams']} t2 ON t2.id = team_id_2
                WHERE tournament_id = :tournament_id AND p.id = :match_id";
        $result = $this->database->get($sql, array(':tournament_id' => $tournament_id, ':match_id' => $match_id));
        return $result ? $result[0] : $result;
    }
}