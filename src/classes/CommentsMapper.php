<?php

namespace Bloodbowl;

use PDO;
use \Monolog\Logger;
use \Bloodbowl\Database;

class CommentsMapper
{
    private $database;
    private $tables;

    public function __construct(PDO $connection, Logger $logger, array $tables)
    {
        $this->database = new Database($connection, $logger);
        $this->tables = $tables;
    }

    /**
     * Posts de un encuentro
     */
    public function getPostComments($post_id)
    {
        $sql = "SELECT * FROM {$this->tables['comments']} WHERE post_id = :post_id AND deleted_at IS NULL AND approved = 1";
        return $this->database->get($sql, array(':post_id' => $post_id));
    }

    public function newComment($values)
    {
        $sql = "INSERT INTO {$this->tables['comments']} (post_id, author, content, approved, created_at)
                VALUES (:post_id, :author, :content, 0, CURRENT_TIMESTAMP)";
        return $this->database->set($sql, $values);
    }
}