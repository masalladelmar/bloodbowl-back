<?php

namespace Bloodbowl;

use \Firebase\JWT\JWT;

//openssl genrsa -out private.key 1024
//openssl rsa -in private.key -pubout > public.key

class Helper
{
    private $privateKey = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQC/fkaksWbQxZ7xjrP0v84EdwKQYluBtaF4uaReoG4DN6AUFFLJ
rdXO0OudCG8WXjoOdxGdTwImi6Vx/yf4OIuY+aib/DWzV+86ve5tQZhDLF81NnM/
xDCwe7kle8kSCeRJ42jatS/cXne5qgb9dbZzTuoKKgAZ4TXBaHZTt3hp7wIDAQAB
AoGBAKe8NqkAc+0fjdBNjbgvYohqrqZGYZ1GugADrMTuhi7vRJsSCkp6qLQWMKxG
vPe85eHRzm4azOlynSMzu5EkBrIeI4LV/I7zb1VEhJ8+3iSFr8JPzXL48qqsUghT
DNxdwOl/iYFIbccwnCPI0qgoDKHnCkXf3lWVDBGrVvXR9pChAkEA6+YRsE8swBDP
Zfm0IkRzNUNVAr7EiWGTLEtKVxYqyU8N1F8Hv7K11vFwQviVi/YZQvzgXv0DSuv3
kzKey9JqCQJBAM/PiifxK7fi8m4qIXeZywHRu5Uy6aKnWwilNoNQgN6BNL5Dmyvc
41LTWnpfaYPt6zQfjRY5deEw4jF7v4MXEjcCQQDiJdHXYE/aYUMy08vo/g4khcQ9
uXzY534b/KHsEwg5rVvouKHJGR5STWv+CCn135wgKCVBavV5IfZaWzD43WNhAkAH
9+xTRcMoZPJo2QK5hFMtea5YtUASNZqfBT7G4EJgxdGQ7iS4FNs+pM4WXSw9m9Tm
BcOyM3far7YssZMRYsQXAkA6csbppveBzeFESeEKgeuVrlvt6si16JkXviyctD3A
OK2nas3iIP/bfuz2/oHwcU9OhnLVVWYFDPXgO7eUjSxm
-----END RSA PRIVATE KEY-----
EOD;

    private $publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/fkaksWbQxZ7xjrP0v84EdwKQ
YluBtaF4uaReoG4DN6AUFFLJrdXO0OudCG8WXjoOdxGdTwImi6Vx/yf4OIuY+aib
/DWzV+86ve5tQZhDLF81NnM/xDCwe7kle8kSCeRJ42jatS/cXne5qgb9dbZzTuoK
KgAZ4TXBaHZTt3hp7wIDAQAB
-----END PUBLIC KEY-----
EOD;

    public function __construct()
    {
    }

    public function slugify($str, $replace = array(), $delimiter = '-')
    {
        if ( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        return $clean;
    }

    public function generateToken($user_id)
    {
        $payload = array(
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + 86400,    // 1 día
            'iss' => 'Ría de Nurgle',
            'aud' => 'Ría de Nurgle',
            'user_id' => $user_id
        );
        $jwt = JWT::encode($payload, $this->privateKey, 'RS256');
        return $jwt;
    }

    public function verifyToken($token)
    {
        $decoded = JWT::decode($token, $this->publicKey, array('RS256'));
        return $decoded;
    }

    function uploadFile($file, $filename, $directory) {
        if (preg_match('/^data:image\/(\w+);base64,/', $file, $type)) {
            $data = substr($file, strpos($file, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif
    
            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }
    
            $data = base64_decode($data);
    
            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }
    
        file_put_contents($directory.$filename, $data);
        return $filename;
    }
}